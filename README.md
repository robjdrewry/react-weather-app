# react-galway-weather
react-galway-weather is a simple SPA for showing weather information for Galway city.

### Usage
```sh
git clone git@gitlab.com:robjdrewry/react-weather-app.git
```
Then, add your [OpenWeatherMap] API key to `.env`.
Finally, get your dependencies and run with yarn:
```sh
cd react-weather-app
yarn
yarn start
```
You should be able to find it on `localhost:3000`

### Screenshot
![](https://i.ibb.co/CKzn2qc/Screen-Shot-2019-01-14-at-14-58-25.png)

### Tech

react-galway-weather uses the following:

* [weather-icons] - icons for the weather
* [OpenWeatherMap] - weather API
* [react-router]
* [react-media]
* [create-react-app]

License
----

[GPLv3]

   [weather-icons]: <https://erikflowers.github.io/weather-icons/>
   [OpenWeatherMap]: https://openweathermap.org/
   [react-router]: https://reacttraining.com/react-router/
   [react-media]: https://github.com/ReactTraining/react-media
   [create-react-app]: https://github.com/facebook/create-react-app
   [GPLv3]: https://www.gnu.org/licenses/gpl-3.0.en.html
