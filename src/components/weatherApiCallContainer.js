import React, { Component } from 'react';
import Layout from './Layout.js';

class WeatherApiCallContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasRetrievedWeatherData: false
        };
        this.weatherForecast = [];
        this.latestWeather = {};
        this.unix_time_mult = 1000;
        this.weatherForecastUrl = `http://api.openweathermap.org/data/2.5/forecast?q=Galway,IE&APPID=${process.env.REACT_APP_API_KEY}&units=metric`;
        this.latestWeatherUrl = `http://api.openweathermap.org/data/2.5/weather?q=Galway,IE&APPID=${process.env.REACT_APP_API_KEY}&units=metric`;
    }

    componentDidMount() {
        Promise.all([
            fetch(this.weatherForecastUrl),
            fetch(this.latestWeatherUrl)
        ])
        .then(([wfResp, lwResp]) => {
            if (wfResp.ok && lwResp.ok)
                return Promise.all([wfResp.json(), lwResp.json()])
            throw new Error('unable to retrieve weather forecast and latest weather from OWM API.');
        })
        .then(([weatherForecastData, latestWeatherData]) => {
            this.weatherForecast = weatherForecastData.list.map(entry => this.extractWeatherInfo(entry));
            this.latestWeather = this.extractWeatherInfo(latestWeatherData);

            this.setState((state, props) => {
                return {
                    hasRetrievedWeatherData: true
                }
            });
        }).catch((error) => {
            console.log(error);
        });
    }

    extractWeatherInfo(apiWeatherObject) {
        let weatherInfo = {};

        weatherInfo.timestamp = apiWeatherObject.dt;
        weatherInfo.weatherId = apiWeatherObject.weather[0].id;
        weatherInfo.weatherDescription = apiWeatherObject.weather[0].description;
        weatherInfo.temperature = apiWeatherObject.main.temp;
        weatherInfo.day = this.getDayOfWeek(apiWeatherObject.dt);
        weatherInfo.time = this.getTime(apiWeatherObject.dt);
        weatherInfo.date = this.getDate(apiWeatherObject.dt);
        weatherInfo.pressure = apiWeatherObject.main.pressure;
        weatherInfo.humidity = apiWeatherObject.main.humidity;
        weatherInfo.windSpeed = apiWeatherObject.wind.speed;
        weatherInfo.windDegrees = this.getRoundedWindDegrees(apiWeatherObject.wind.deg);
        weatherInfo.cloudiness = apiWeatherObject.clouds.all;

        return weatherInfo;
    }

    getDayOfWeek(datetime) {
        let integerDay = new Date(datetime * this.unix_time_mult).getDay();

        switch (integerDay) {
            case 0:
                return 'Sun';
            case 1:
                return 'Mon';
            case 2:
                return 'Tue';
            case 3:
                return 'Wed';
            case 4:
                return 'Thu';
            case 5:
                return 'Fri';
            case 6:
                return 'Sat';
            default:
                return '';
        }
    }

    getTime(datetime) {
        return new Date(datetime * this.unix_time_mult).toTimeString().slice(0, 5);
    }

    getDate(datetime) {
        let dayOfMonth = new Date(datetime * this.unix_time_mult).getDate();
        let integerMonth = new Date(datetime * this.unix_time_mult).getMonth();
        
        let month = ((integer) => {
            switch(integer) {
                case 0:
                    return 'January';
                case 1:
                    return 'February';
                case 2:
                    return 'March';
                case 3:
                    return 'April';
                case 4:
                    return 'May';
                case 5:
                    return 'June';
                case 6:
                    return 'July';
                case 7:
                    return 'August';
                case 8:
                    return 'September';
                case 9:
                    return 'October';
                case 10:
                    return 'November';
                case 11:
                    return 'December';
                default:
                    return '';
            }
        })(integerMonth);

        return dayOfMonth + ' / ' + month;
    }

    getRoundedWindDegrees(degrees) {
        return Math.round(degrees);
    }
  
    render() {
        if (this.state.hasRetrievedWeatherData) {
            return <Layout latestWeather={this.latestWeather} weatherForecast={this.weatherForecast} />
        } else {
            return <div>fetching weather data...</div>
        }
    }
}

export default WeatherApiCallContainer;