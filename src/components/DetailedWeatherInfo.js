import React from 'react';
import './detailed-weather-info.css';
import '../weather-icons/css/weather-icons.min.css';
import '../weather-icons/css/weather-icons-wind.min.css';

const DetailedWeatherInfo = ({ location, children }) => (
    <div className="detailed-weather-info">
        {children}
        <div className="day-and-date">
            {location.state.day} • {location.state.date}
        </div>
        <div className="time">
            {location.state.time}
        </div>
        <div className="weather-icon">
            <i className={`wi wi-owm-${location.state.weatherId}`}></i>
        </div>
        <div className="weather-description">
            {location.state.weatherDescription}
        </div>
        <div className="detailed-info-grid">
            <i className="wi wi-thermometer"></i>
            <div className="label temperature-label">Temperature</div>
            <div className="weather-reading temperature-reading">{location.state.temperature}°C</div>
            
            <i className="wi wi-barometer"></i>
            <div className="label pressure-label">Pressure</div>
            <div className="weather-reading pressure-reading">{location.state.pressure}hPa</div>
            
            <i className="wi wi-humidity"></i>
            <div className="label humidity-label">Humidity</div>
            <div className="weather-reading humidity-reading">{location.state.humidity}%</div>
            
            <i className="wi wi-strong-wind"></i>
            <div className="label wind-speed-label">Wind speed</div>
            <div className="weather-reading speed-reading">{location.state.windSpeed}m/s</div>
            
            <i className={`wi wi-wind towards-${location.state.windDegrees}-deg`}></i>
            <div className="label wind-direction-label">Wind degrees</div>
            <div className="weather-reading wind-direction-reading">{location.state.windDegrees}°</div>
            
            <i className="wi wi-cloud"></i>
            <div className="label cloudiness-label">Cloudiness</div>
            <div className="weather-reading cloudiness-reading">{location.state.cloudiness}%</div>
        </div>
    </div>
);

export default DetailedWeatherInfo;