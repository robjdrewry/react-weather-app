import React from 'react';
import { Link } from 'react-router-dom';
import './weather-tile.css'
import '../weather-icons/css/weather-icons.min.css';

const WeatherTile = ({ weatherInfo }) => (
    <Link to={{
        pathname: "/detailedweatherinfo/",
        state: {...weatherInfo}
    }}>
        <div className="weather-tile">
            <div className="weather-day">{weatherInfo.day}</div>
            <div className="weather-time">{weatherInfo.time}</div>
            <i className={`wi wi-owm-${weatherInfo.weatherId}`} title={`${weatherInfo.weatherDescription}`}></i>
            <div className="weather-temperature">{weatherInfo.temperature}°C</div>
        </div>
    </Link>
)

export default WeatherTile;